import json
import numpy as np
import pandas as pd
from alive_progress import alive_bar
from intentron.utils import *


def check_stage(src):
    output = True
    for gesture in os.listdir(src):
        if osp.isdir(osp.join(src, gesture)):
            for side in os.listdir(osp.join(src, gesture)):
                if osp.isdir(osp.join(src, gesture, side)):
                    for trial in os.listdir(osp.join(src, gesture, side)):
                        if osp.isdir(osp.join(src, gesture, side, trial)):
                            for mode in os.listdir(osp.join(src, gesture, side, trial)):
                                folder = osp.join(src, gesture, side, trial)
                                files = get_files(osp.join(src, gesture, side, trial, mode))
                                print(folder, len(files))
                                output *= len(files) == 120

    if output:
        log.info("Checking success")
    else:
        log.info("Checking failure")
