import os
import os.path as osp
import shutil
from loguru import logger

logger.add("pipeline_{time}.log", format="{time} - {level} - {message}", level="DEBUG")
log = logger


def rm(folder: str):
    if os.path.exists(folder):
        if osp.isfile(folder):
            os.remove(folder)
        else:
            shutil.rmtree(folder)


def copy(src, dst):
    if os.path.isfile(src):
        shutil.copy(src, dst)
    else:
        for src_folder, _, files in os.walk(src):
            dst_folder = src_folder.replace(src, dst)
            touch(dst_folder)
            for f in files:
                file = os.path.join(src_folder, f)
                shutil.copy(file, dst_folder)


def move(src, dst):
    if os.path.isfile(src):
        shutil.move(src, dst)
    else:
        for src_folder, _, files in os.walk(src):
            dst_folder = src_folder.replace(src, dst)
            touch(dst_folder)
            for f in files:
                file = os.path.join(src_folder, f)
                shutil.move(file, dst_folder)


def touch(folder: str):
    # shutil.rmtree(path, ignore_errors=True)
    os.makedirs(folder, exist_ok=True)
