import setuptools


setuptools.setup(
    name='intentron',
    version='0.2.0',
    author='dmitrynvm',
    author_email='dmitrynvm@gmail.com',
    description='A Toolkit for Deep Learning in Gesture recognition and intention prediction',
    long_description='',
    long_description_content_type='text/markdown',
    url='https://www.gitlab.com/dmitrynvm/intentron',
    keywords=['Deep Learning', 'Gesture recognition', 'Intention prediction', 'AI'],
    packages=setuptools.find_packages(exclude=('tests',)),
    package_data={},
    install_requires=[
        'numpy>=1.16.6',
        'click>=8.1.3',
        'opencv-python>=4.5.5',
        'alive-progress>=2.4.1',
        'pandas>=1.4.2',
        'matplotlib>=3.5.2',
        'scikit-learn>=1.1.1',
        'scipy>=1.8.1',
        'mediapipe>=0.8.10'
    ],
    extras_require={},
    requires_python='>=3.6',
    classifiers=[
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.6',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
