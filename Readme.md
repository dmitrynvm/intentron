# Human motion recognition and intention prediction
## Command line utility for data pipeline automation

1. Installation
```
pip3 install poetry
python3 -m venv cv
source cv/bin/activate
git clone https://gitlab.com/dmitrynvm/intentron
cd intentron
poetry install
alias intentron="python3 -m intentron"
```
cd intentron
pip3 install -e .

1. Prepare
```
intentron prepare SOURCE DEST [FMT]
```

2. Parse
```
intentron parse SOURCE DEST
```
